package id.ac.ui.cs.advprog.tutorial5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class SoulTest {
    private Soul soulTest;

    @BeforeEach
    public void setUp(){
        soulTest = new Soul();
        soulTest.setAge(15);
        soulTest.setGender("M");
        soulTest.setId(21);
        soulTest.setName("Test Soul");
        soulTest.setOccupation("Mahasiswa");
    }

    @Test
    void getGender() {
        assertEquals("M",soulTest.getGender());
    }

    @Test
    void setGender() {
        soulTest.setGender("L");
        assertEquals("L",soulTest.getGender());
    }

    @Test
    void getOccupation() {
        assertEquals("Mahasiswa",soulTest.getOccupation());
    }

    @Test
    void setOccupation() {
        soulTest.setOccupation("Magang");
        assertEquals("Magang",soulTest.getOccupation());
    }

    @Test
    void getName() {
        assertEquals("Test Soul",soulTest.getName());
    }

    @Test
    void setName() {
        soulTest.setName("Soull");
        assertEquals("Soull",soulTest.getName());
    }

    @Test
    void getAge() {
        assertEquals(15,soulTest.getAge());
    }

    @Test
    void setAge() {
        soulTest.setAge(21);
        assertEquals(21,soulTest.getAge());
    }

    @Test
    void getId() {
        assertEquals(21,soulTest.getId());
    }

    @Test
    void setId() {
        soulTest.setId(2);
        assertEquals(2,soulTest.getId());
    }

}
