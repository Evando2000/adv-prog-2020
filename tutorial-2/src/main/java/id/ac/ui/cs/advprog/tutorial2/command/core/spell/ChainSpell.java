package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spellArrayList = new ArrayList<>();

    public ChainSpell(ArrayList<Spell> arrayList) {
        this.spellArrayList = arrayList;
	}

	@Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for (Spell spellIni : spellArrayList) {
            spellIni.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = spellArrayList.size()-1; i >= 0; i--) {
            this.spellArrayList.get(i).cast();
        }
    }
}
