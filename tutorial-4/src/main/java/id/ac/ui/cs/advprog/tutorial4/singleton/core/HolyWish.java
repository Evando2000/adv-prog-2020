package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {
    private static HolyWish wishObj;
    private String wish;

    // TODO complete me with any Singleton approach
    private HolyWish(){
        wish="";
    }

    public static HolyWish getInstance() {
        if(wishObj == null){
            wishObj = new HolyWish();
        }
        return wishObj;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
