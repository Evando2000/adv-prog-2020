package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    private String thisName;
    private String thisRole;
    private List<Member> childList = new ArrayList<>();

    public OrdinaryMember(String ordName, String ordRole) {
        thisName = ordName;
        thisRole = ordRole;
    }

    @Override
    public String getName() {
        return thisName;
    }

    @Override
    public String getRole() {
        return thisRole;
    }

    @Override
    public void addChildMember(Member member) {
        // Do Nothing
    }

    @Override
    public void removeChildMember(Member member) {
        // Do Nothing
    }

    @Override
    public List<Member> getChildMembers() {
        return childList;
    }
    //TODO: Complete me
}
