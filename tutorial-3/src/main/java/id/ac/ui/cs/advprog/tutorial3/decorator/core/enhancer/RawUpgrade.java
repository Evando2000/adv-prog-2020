package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return "Raw " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int upVal = new Random().nextInt(6) + 5;
        if(weapon != null){
            return weapon.getWeaponValue() + upVal;
        }else{
            return upVal;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Raw " + weapon.getDescription();
    }
}
