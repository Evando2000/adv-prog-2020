package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return "Chaos " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int upVal = new Random().nextInt(6) + 50;
        if(weapon != null){
            return weapon.getWeaponValue() + upVal;
        }else{
            return upVal;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Chaos " + weapon.getDescription();
    }
}
