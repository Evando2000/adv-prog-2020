package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return "Regular " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int upVal = new Random().nextInt(5) + 1;
        if(weapon != null){
            return weapon.getWeaponValue() + upVal;
        }else{
            return upVal;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Regular " + weapon.getDescription();
    }
}
