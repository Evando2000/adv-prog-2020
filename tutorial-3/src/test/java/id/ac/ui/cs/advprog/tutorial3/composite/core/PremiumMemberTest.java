package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Aqua", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        OrdinaryMember testMem = new OrdinaryMember("testMem1", "testRole1");
        member.addChildMember(testMem);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        OrdinaryMember testMem = new OrdinaryMember("testMem1", "testRole1");
        member.addChildMember(testMem);
        member.removeChildMember(testMem);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        OrdinaryMember testMem1 = new OrdinaryMember("testMem1", "testRole1");
        OrdinaryMember testMem2 = new OrdinaryMember("testMem2", "testRole2");
        OrdinaryMember testMem3 = new OrdinaryMember("testMem3", "testRole3");
        OrdinaryMember testMem4 = new OrdinaryMember("testMem4", "testRole4");
        member.addChildMember(testMem1);
        member.addChildMember(testMem2);
        member.addChildMember(testMem3);
        member.addChildMember(testMem4);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member masterTest = new PremiumMember("MasterTest", "Master");
        OrdinaryMember testMem1 = new OrdinaryMember("testMem1", "testRole1");
        OrdinaryMember testMem2 = new OrdinaryMember("testMem2", "testRole2");
        OrdinaryMember testMem3 = new OrdinaryMember("testMem3", "testRole3");
        OrdinaryMember testMem4 = new OrdinaryMember("testMem4", "testRole4");
        masterTest.addChildMember(testMem1);
        masterTest.addChildMember(testMem2);
        masterTest.addChildMember(testMem3);
        masterTest.addChildMember(testMem4);
        assertEquals(4, masterTest.getChildMembers().size());
    }
}
