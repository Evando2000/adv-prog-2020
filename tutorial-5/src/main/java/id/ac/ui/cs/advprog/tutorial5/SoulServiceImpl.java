package id.ac.ui.cs.advprog.tutorial5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// TODO: Import service bean

@Service
public class SoulServiceImpl implements SoulService {
    // TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service

    @Autowired
    private SoulRepository soulRep;

    public SoulServiceImpl(SoulRepository soulRepo) {
        soulRep = soulRepo;
    }

    @Override
    public List<Soul> findAll() {
        return soulRep.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return soulRep.findById(id);
    }

    @Override
    public void erase(Long id) {
        soulRep.deleteById(id);
    }

    @Override
    public Soul rewrite(Soul soul) {
        return soulRep.save(soul);
    }

    @Override
    public Soul register(Soul soul) {
        return soulRep.save(soul);
    }
}
