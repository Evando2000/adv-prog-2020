package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class DrangleicArmory implements Armory {

    @Override
    public Armor craftArmor() {
        // TODO fix me
        MetalArmor metArm = new MetalArmor();
        return metArm;
    }

    @Override
    public Weapon craftWeapon() {
        // TODO fix me
        ShiningBuster shinBust = new ShiningBuster();
        return shinBust;
    }

    @Override
    public Skill learnSkill() {
        // TODO fix me
        ShiningForce shinFor = new ShiningForce();
        return shinFor;
    }
}
