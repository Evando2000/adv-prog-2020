package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        int sizeGuild = guild.getMemberList().size();
        Member memberOrd = new OrdinaryMember("testMem", "Slave");
        guild.addMember(guildMaster, memberOrd);
        assertEquals(guild.getMemberList().size(), sizeGuild+1);
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member memberOrd2 = new OrdinaryMember("testMem", "Slave");
        guild.addMember(guildMaster, memberOrd2);
        int sizeGuild2 = guild.getMemberList().size();
        guild.removeMember(guildMaster, memberOrd2);
        assertEquals(guild.getMemberList().size(), sizeGuild2-1);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member memberOrd3 = new OrdinaryMember("testMem", "Slave");
        guild.addMember(guildMaster, memberOrd3);
        assertEquals(memberOrd3, guild.getMember("testMem", "Slave"));
    }
}
