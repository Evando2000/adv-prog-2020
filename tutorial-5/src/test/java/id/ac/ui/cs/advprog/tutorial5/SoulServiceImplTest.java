package id.ac.ui.cs.advprog.tutorial5;

import id.ac.ui.cs.advprog.tutorial5.Soul;
import id.ac.ui.cs.advprog.tutorial5.SoulRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {
    @Mock
    private SoulRepository soulRepository;

    private Soul soulTest;

    @InjectMocks
    private SoulServiceImpl soulService;

    @BeforeEach
    public void setUp(){
        soulTest = new Soul();
        soulTest.setAge(15);
        soulTest.setGender("M");
        soulTest.setId(1);
        soulTest.setName("Test Soul");
        soulTest.setOccupation("Mahasiswa");
    }

    @Test
    public void testFindAll(){
        List<Soul> soulList = soulService.findAll();
        lenient().when(soulService.findAll()).thenReturn(soulList);
    }

    @Test
    public void testRegisterSoul(){
        soulService.register(soulTest);
        lenient().when(soulService.register(soulTest)).thenReturn(soulTest);
    }

    @Test
    public void testFindSoul(){
        soulService.register(soulTest);
        Optional<Soul> optionalSoulExist = soulService.findSoul(soulTest.getId());
        Optional<Soul> optionalSoulDoesntExist = soulService.findSoul((long) 2);
        lenient().when(soulService.findSoul(soulTest.getId())).thenReturn(Optional.of(soulTest));
    }

    @Test
    public void testErase(){
        soulService.register(soulTest);
        soulService.erase(soulTest.getId());
        lenient().when(soulService.findSoul(soulTest.getId())).thenReturn(Optional.empty());
    }

    @Test
    public void testRewriteSoul(){
        lenient().when(soulService.rewrite(soulTest)).thenReturn(soulTest);
    }

}
