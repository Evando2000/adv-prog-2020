package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return "Unique " + weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int upVal = new Random().nextInt(6) + 10;
        if(weapon != null){
            return weapon.getWeaponValue() + upVal;
        }else{
            return upVal;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Unique " + weapon.getDescription();
    }
}
