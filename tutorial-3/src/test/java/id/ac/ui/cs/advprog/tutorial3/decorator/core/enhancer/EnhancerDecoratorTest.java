package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        //TODO: Complete me
        int weaponValue = weapon1.getWeaponValue();
        assertTrue(weaponValue >= 50 && weaponValue <= 55);

        weaponValue = weapon2.getWeaponValue();
        assertTrue(weaponValue >= 15 && weaponValue <= 20);

        weaponValue = weapon3.getWeaponValue();
        assertTrue(weaponValue >= 1 && weaponValue <= 5);

        weaponValue = weapon4.getWeaponValue();
        assertTrue(weaponValue >= 5 && weaponValue <= 10);

        weaponValue = weapon5.getWeaponValue();
        assertTrue(weaponValue >= 10 && weaponValue <= 15);
    }

}
