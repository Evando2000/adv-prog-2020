package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy = new LordranAcademy();
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof Knight);
        assertTrue(metalClusterKnight instanceof Knight);
        assertTrue(syntheticKnight instanceof Knight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        majesticKnight.setName("Shining Majestic");
        metalClusterKnight.setName("Shimmering Metal");
        syntheticKnight.setName("Splendid Synthetic");
        assertEquals("Shining Majestic", majesticKnight.getName());
        assertEquals("Shimmering Metal", metalClusterKnight.getName());
        assertEquals("Splendid Synthetic", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals("Thousand Jacker", majesticKnight.getWeapon().getName());
        assertEquals("Shining Armor", majesticKnight.getArmor().getName());

        assertEquals("Shining Armor", metalClusterKnight.getArmor().getName());
        assertEquals("Thousand Years of Pain", metalClusterKnight.getSkill().getName());

        assertEquals("Thousand Jacker", syntheticKnight.getWeapon().getName());
        assertEquals("Thousand Years of Pain", syntheticKnight.getSkill().getName());
    }
}
