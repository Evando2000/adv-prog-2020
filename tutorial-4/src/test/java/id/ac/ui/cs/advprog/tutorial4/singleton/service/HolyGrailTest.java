package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    // TODO create tests
    private HolyGrail testHolyGrail;

    @BeforeEach
    public void setUp(){
        testHolyGrail = new HolyGrail();
    }

    @Test
    public void testGetHolyWish(){
        assertNotNull(testHolyGrail.getHolyWish());
    }

    @Test
    public void testRunHolyWish(){
        testHolyGrail = spy(testHolyGrail);
        testHolyGrail.makeAWish("");
        verify(testHolyGrail, atLeastOnce()).makeAWish("");
    }
}
