package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String thisName;
    private String thisRole;
    private List<Member> childList = new ArrayList<>();

    public PremiumMember(String premName, String premRole) {
        thisName = premName;
        thisRole = premRole;
    }

    @Override
    public String getName() {
        return thisName;
    }

    @Override
    public String getRole() {
        return thisRole;
    }

    @Override
    public void addChildMember(Member member) {
        if(thisRole.equals("Master") || childList.size() < 3){
            childList.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        childList.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return childList;
    }
    //TODO: Complete me
}
