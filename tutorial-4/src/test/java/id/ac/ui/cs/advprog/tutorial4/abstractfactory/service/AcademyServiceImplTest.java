package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    private AcademyRepository academyRepository;
    private AcademyService academyService;

    // TODO create tests

    @BeforeEach
    public void setUp(){
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void testGetListOfAcademy(){
        assertEquals(2, academyService.getKnightAcademies().size());
    }

    @Test
    public void testProduceAndRemoveKnight(){
        academyService.produceKnight("Drangleic","synthetic");
        Knight testKnight = academyService.getKnight();
        testKnight.setName("Drangleic Synthetic Knight");
        assertNotNull(testKnight);
        assertEquals("Drangleic Synthetic Knight", testKnight.getName());
    }
}
